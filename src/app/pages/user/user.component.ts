import { AlertsService } from './../../services/alerts.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";

declare const getAyoba: any;
declare const onLocationChanged: any;

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {
    userName;
    currentPresence = 0;
    currentNickname: any;
    currentAvatarPath: any;
    longitude: any;
    latitude: any;
    public Ayoba = getAyoba();
    constructor(private alertService: AlertsService,
                private toastService: ToastrService) {}
    ngOnInit() {
    }

  onPresenceChanged(presence) {
    if (presence != null) {
      this.currentPresence = presence
    }
  }

  onProfileChanged(nickname, avatarPath) {
    if (nickname != null && avatarPath != null) {
      this.currentNickname = nickname
      this.currentAvatarPath = avatarPath
    }
  }
  onAlert() {
    var Ayoba = getAyoba()

    var fdi = this.alertService.getFireDangerIndex();

    if (fdi > 0 && fdi <= 45) {
      Ayoba.sendMessage('Alertify status: No need to leave your home, fire stage is currently at safe');
      Ayoba.sendMessage(`Fire danger index is currently at ${this.alertService.getFireDangerIndex()}`);
    } else if (fdi > 45 && fdi <= 75) {
      Ayoba.sendMessage('Keep a constant watch for unexpected wind speed and direction changes. Fire Stage is currently at warning level');
      Ayoba.sendMessage(`Fire danger index is currently at ${this.alertService.getFireDangerIndex()}`);
    } else {
      Ayoba.sendMessage(`All personnel and equipment should be removed from field. Fire teams, labour and equipment are to be placed on full standby. At the first sign of smoke, every possible measure should be taken in order to bring the fire under control in the shortest possible time. All available fire trucks are to be called for without delay`);
      Ayoba.sendMessage(`Fire danger index is currently at ${this.alertService.getFireDangerIndex()}`);
    }

    this.toastService.success('Alert sent!', 'A notification will be sent automatically to everyone on your contact list');
  }

  async sos() {
      await this.Ayoba.sendMessage('I need help! My coordinates are: ');

      if (this.latitude !== undefined && this.longitude !== undefined) {
        this.Ayoba.sendMessage(`Longitude: ${this.longitude}`);
        this.Ayoba.sendMessage(`Latitude: ${this.latitude}`);
      } else {
        this.Ayoba.sendMessage('There is currently a problem with sending the current location emergency services have been called. Please do a follow up');
      }
  }

  onLocationChanged(lat, lon) {
    if (lat != null && lon != null) {
      this.latitude  = lat;
      this.longitude = lon;
    }
  }
}
