import { Component,OnInit } from '@angular/core';

declare var google: any;
declare var getAyoba;
@Component({
    moduleId: module.id,
    selector: 'maps-cmp',
    templateUrl: 'maps.component.html'
})

export class MapsComponent implements OnInit {
    city: string;
    public Ayoba = getAyoba();

    ngOnInit() {
      var myLatlng = new google.maps.LatLng(-26.102332924, 28.090999636);
      var sowetoLatlng = new google.maps.LatLng(-26.266111, 27.865833);

        var mapOptions = {
          zoom: 13,
          center:  myLatlng,
          scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
          styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]

        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            title:"Hello World!"
        });

      var sowetoMarker = new google.maps.Marker({
        position: sowetoLatlng,
        title: "Hello World!"
      });


        // To add the marker to the map, call setMap();
        marker.setMap(map);
        sowetoMarker.setMap(map);

    }

    onShare() {
      this.Ayoba.sendMessage('In case of a fire go to the fire sensor in your area for protection! Sensor coordinates are: ');
      this.Ayoba.sendMessage('Longitude: -26.102332924');
      this.Ayoba.sendMessage('Latitude: 28.090999636');
    }
}
