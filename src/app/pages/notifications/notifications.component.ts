import { Component } from '@angular/core';
import { ToastrService } from "ngx-toastr";


@Component({
    selector: 'notifications-cmp',
    moduleId: module.id,
    templateUrl: 'notifications.component.html'
})

export class NotificationsComponent{
  constructor(private toastr: ToastrService) {}

  public colourIndications = [
    {info: 'Fires are not likely to ignite. If they do, they are likely to go out without suppression action. There is little flaming combustion.'},
  {info: ' Fires likely to ignite readily but spread slowly.'}]
  showNotification(from, align) {

        this.toastr.show(
        '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">Welcome to <b>Paper Dashboard Angular</b> - a beautiful bootstrap dashboard for every web developer.</span>',
          "",
          {
            timeOut: 4000,
            closeButton: true,
            enableHtml: true,
            toastClass: "alert alert-primary alert-with-icon",
            positionClass: "toast-" + from + "-" + align
          }
        );
  }

  onClose(index: any) {
    this.colourIndications.splice(index, 1);
  }
}
