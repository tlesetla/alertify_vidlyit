import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  private fireDanger;

  constructor() { }

  setFireDangerIndex(fdi: number) {
    this.fireDanger = fdi;
  }

  getFireDangerIndex() {
    return this.fireDanger;
  }
}
