import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }
  url = 'https://api.weatherbit.io/v2.0/current?city=Pretoria&key=3bdbebcdffe343ceab78a3ef1b1678b9';

  getWeather(): Observable<any> {
    return this.http.get<any>(this.url);
  }
}
